#!/usr/bin/make -f

all: assets/bootstrap.min.css assets/bootstrap.bundle.min.js

BOOTSTRAP_VERSION=5
BOOTSWATCH_VERSION=5

assets/bootstrap.min.css:
	curl -o "$@" "https://cdn.jsdelivr.net/npm/bootswatch@${BOOTSWATCH_VERSION}/dist/darkly/bootstrap.min.css"

assets/bootstrap.bundle.min.js:
	curl -o "$@" "https://cdn.jsdelivr.net/npm/bootstrap@${BOOTSTRAP_VERSION}/dist/js/bootstrap.bundle.min.js"


clean:
	rm -f assets/bootstrap.*
