from dash import Dash, html, dcc, Input, Output, State, ctx, ALL
import requests
from datetime import datetime, timedelta

app = Dash(
    __name__,
    requests_pathname_prefix="/app/",
    external_stylesheets=["/app/assets/bootstrap.min.css"],
    assets_folder="../../assets/",
)

templates_div = html.Div(
    id="templates", className="d-flex gap-2 flex-wrap", children=[]
)
app.layout = html.Div(
    className="container-md",
    children=[
        html.Div(
            children=[
                html.Label("Label Text", className="form-label"),
                dcc.Textarea(
                    id="label-text",
                    style={"width": "100%"},
                    className="form-control form-control-lg",
                    placeholder="Text for label",
                    rows="5",
                ),
                html.Div(
                    id="label-help",
                    className="form-text",
                    children=["Multiple lines print as shown"],
                ),
            ],
        ),
        html.Div(
            className="row",
            children=[
                html.Div(
                    className="col-sm-2",
                    children=[
                        html.Label("Font size", className="form-label"),
                        dcc.Input(
                            id="font-size",
                            className="form-control",
                            type="number",
                            min=0,
                            max=100,
                        ),
                        html.Div(
                            id="emailHelp",
                            className="form-text",
                            children=["Leave blank for auto font size"],
                        ),
                    ],
                ),
                html.Div(
                    className="col-sm-2",
                    children=[
                        html.Label("Copies", className="form-label"),
                        dcc.Input(
                            id="copies",
                            className="form-control",
                            value=1,
                            type="number",
                            min=0,
                            max=100,
                        ),
                    ],
                ),
                html.Div(
                    className="col-sm-8 align-self-center",
                    children=[
                        html.Button(
                            "Print",
                            name="print",
                            id="print",
                            type="submit",
                            className="btn btn-primary btn-lg",
                        )
                    ],
                ),
            ],
        ),
        html.Div(
            className="row", children=[html.Div(id="print-result", className="mb-3")]
        ),
        html.Div(
            id="template-section",
            className="container",
            children=[
                html.Label("Form Templates", className="form-label"),
                templates_div,
            ],
        ),
    ],
)


@app.callback(
    Output("print-result", "children"),
    Output("copies", "value"),
    Input("print", "n_clicks"),
    State("label-text", "value"),
    State("font-size", "value"),
    State("copies", "value"),
    prevent_initial_call=True,
)
def print_button(print, label_text, font_size, copies):
    if ctx.triggered_id != "print":
        return
    # fix up the label text
    queue_item = dict(text=label_text.split("\n"), type="text", copies=copies)

    if font_size is not None:
        queue_item["fontsize"] = int(font_size)

    res = requests.post("http://localhost:8000/v1/queue", json=queue_item)
    return res.text, 1


def today_tomorrow_template(template: str) -> str:
    today = datetime.now()
    tomorrow = today + timedelta(days=1)
    return template.format(
        today=today.strftime("%b %d"), tomorrow=tomorrow.strftime("%b %d")
    )


templates = {
    "bottle": {
        "label": "Bottle",
        "function": today_tomorrow_template,
        "template": "4% Cow Milk\n 5 floz\nuse {tomorrow}",
    },
    "rayleigh": {"label": "Rayleigh", "value": "Rayleigh\nArmstrong-\nDodgen"},
    "kepler": {"label": "Kepler", "value": "Kepler\nArmstrong-\nDodgen"},
    "puree": {
        "label": "Purée",
        "function": today_tomorrow_template,
        "template": "\nPurée\nuse {tomorrow}",
    },
    "cracker": {
        "label": "Cracker",
        "function": today_tomorrow_template,
        "template": "Graham\nCracker\nuse {tomorrow}",
    },
    "strawberries": {
        "label": "Strawberries",
        "function": today_tomorrow_template,
        "template": "Sliced\nStrawberries\nuse {tomorrow}",
    },
    "cheese": {
        "label": "Cheese",
        "function": today_tomorrow_template,
        "template": "Cheese\nSticks\nuse {tomorrow}",
    },
    "carrots": {
        "label": "Carrots",
        "function": today_tomorrow_template,
        "template": "Cooked\nCarrots\nuse {tomorrow}",
    },
    "sunbutter": {
        "label": "Sunbutter",
        "function": today_tomorrow_template,
        "template": "Sunbutter\nJelly\nuse {tomorrow}",
    },
    "pasta": {
        "label": "Pasta",
        "function": today_tomorrow_template,
        "template": "Pasta\n\nuse {tomorrow}",
    },
}

for name, template in templates.items():
    templates_div.children.append(
        html.Button(
            template["label"],
            name=name,
            id={"type": "template_button", "name": name},
            className="btn btn-secondary",
        )
    )


@app.callback(
    Output("label-text", "value"),
    Input({"type": "template_button", "name": ALL}, "n_clicks"),
)
def template_button(button):
    if ctx.triggered_id is None:
        return
    if "name" not in ctx.triggered_id:
        return
    name = ctx.triggered_id["name"]
    if name not in templates:
        return
    template = templates[name]
    if "value" in template:
        return template["value"]
    if "function" in template:
        template_args = {
            key: template[key]
            for key in template
            if key not in {"function", "name", "value", "label"}
        }
        return template["function"](**template_args)
    return "No template defined"
